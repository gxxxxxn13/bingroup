# coding: utf-8
from sqlalchemy import BigInteger, Boolean, Column, Date, Float, Integer, SmallInteger, Table, Text
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Basicblock(Base):
    __tablename__ = 'basicblock'

    id = Column(Integer, primary_key=True)
    functionid = Column(Integer)
    address1 = Column(BigInteger)
    address2 = Column(BigInteger)
    algorithm = Column(SmallInteger)
    evaluate = Column(Boolean)


class Basicblockalgorithm(Base):
    __tablename__ = 'basicblockalgorithm'

    id = Column(SmallInteger, primary_key=True)
    name = Column(Text)


t_file = Table(
    'file', metadata,
    Column('id', Integer),
    Column('filename', Text),
    Column('exefilename', Text),
    Column('hash', Text(40)),
    Column('functions', Integer),
    Column('libfunctions', Integer),
    Column('calls', Integer),
    Column('basicblocks', Integer),
    Column('libbasicblocks', Integer),
    Column('edges', Integer),
    Column('libedges', Integer),
    Column('instructions', Integer),
    Column('libinstructions', Integer)
)


class Function(Base):
    __tablename__ = 'function'

    id = Column(Integer, primary_key=True)
    address1 = Column(BigInteger)
    address2 = Column(BigInteger)
    similarity = Column(Float)
    confidence = Column(Float)
    flags = Column(Integer)
    algorithm = Column(SmallInteger)
    evaluate = Column(Boolean)
    commentsported = Column(Boolean)
    basicblocks = Column(Integer)
    edges = Column(Integer)
    instructions = Column(Integer)


class Functionalgorithm(Base):
    __tablename__ = 'functionalgorithm'

    id = Column(SmallInteger, primary_key=True)
    name = Column(Text)


t_instruction = Table(
    'instruction', metadata,
    Column('basicblockid', Integer),
    Column('address1', BigInteger),
    Column('address2', BigInteger)
)


t_metadata = Table(
    'metadata', metadata,
    Column('version', Text),
    Column('file1', Integer),
    Column('file2', Integer),
    Column('description', Text),
    Column('created', Date),
    Column('modified', Date),
    Column('similarity', Float),
    Column('confidence', Float)
)
