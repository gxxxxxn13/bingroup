# -*- coding: utf-8 -*-
import sys, os, shutil
import pefile, hashlib, bindiff, time

# group it if over this percentage
BINDIFF_GROUPING_SIMILARITY = 0.80
TASK_NAME = 'test_task'

def get_md5(fname):
	hash_md5 = hashlib.md5()
	with open(fname, "rb") as f:
		for chunk in iter(lambda: f.read(4096), b""):
			hash_md5.update(chunk)
	return hash_md5.hexdigest()

def get_epoch(pe):
	epoch = time.gmtime(pe.FILE_HEADER.TimeDateStamp)
	return epoch

def get_timestamp(target):
	pe = pefile.PE(target)
	epoch = get_epoch(pe)
	timestamp = time.strftime('%Y-%m-%d %H:%M:%S', epoch)
	return timestamp

def get_imphash(target):
	pe = pefile.PE(target)
	imphash = pe.get_imphash()
	return imphash

def group_by_imphash(file_list):
	imphash_group = {}
	for target_filepath in file_list:
		imphash = None
		try:
			imphash = get_imphash(target_filepath)
		except:
			# not pe pass
			pass
			continue

		if imphash not in imphash_group:
			imphash_group[imphash] = []

		imphash_group[imphash].append(target_filepath)
	return imphash_group

def get_simple_reverse_imphash_group(imphash_group):
	simple_reverse_imphash_group = {}
	for imphash in imphash_group:
		if imphash not in simple_reverse_imphash_group.values():
			simple_reverse_imphash_group[imphash_group[imphash][0]] = imphash
	return simple_reverse_imphash_group

def group_by_bindiff(sample_list):
	diff_result = []
	for i in xrange(len(sample_list)):
		for j in xrange(i+1, len(sample_list)):
			if i == j:
				continue
			diff_file = bindiff.binary2bindiff(sample_list[i], sample_list[j])
			similarity = bindiff.get_similarity_from_bindiff(diff_file)
			diff_result.append((sample_list[i], sample_list[j], similarity))
	return diff_result

def data_normalize(folder):
	return os.path.join(bindiff.BINDIFF_PYTHON_WORK_PATH, TASK_NAME)

if __name__ == '__main__':
	reload(sys)
	sys.setdefaultencoding('utf-8')

	if len(sys.argv) < 2:
		print 'Usage: {} folder'.format(sys.argv[0])
		exit()

	folder = data_normalize(os.path.abspath(sys.argv[1]))

	file_list = []
	for f in os.listdir(folder):
		file_list.append(os.path.join(folder, f))

	# goruping by imphash to reduce bindiff times
	imphash_group = group_by_imphash(file_list)
	simple_reverse_imphash_group = get_simple_reverse_imphash_group(imphash_group)
	sample_list = simple_reverse_imphash_group.keys()
	diff_result = group_by_bindiff(sample_list)
	imphash_group_sets = []
	set_mapping = {}
	set_idx = 0

	# grouping imphash by bindiff result
	for diff in diff_result:
		if diff[2] > BINDIFF_GROUPING_SIMILARITY:
			print simple_reverse_imphash_group[diff[0]], simple_reverse_imphash_group[diff[1]], diff[2]
			if not simple_reverse_imphash_group[diff[0]] in set_mapping:
				set_mapping[simple_reverse_imphash_group[diff[0]]] = set_idx
				set_mapping[simple_reverse_imphash_group[diff[1]]] = set_idx
				set_idx += 1
				s = set()
				s.add(simple_reverse_imphash_group[diff[0]])
				s.add(simple_reverse_imphash_group[diff[1]])
				imphash_group_sets.append(s)
			else:
				parent_idx = set_mapping[simple_reverse_imphash_group[diff[0]]]
				parent_set = imphash_group_sets[parent_idx]
				set_mapping[simple_reverse_imphash_group[diff[1]]] = parent_idx
				parent_set.add(simple_reverse_imphash_group[diff[1]])
				imphash_group_sets[parent_idx] = parent_set

	orphand_imphash_list = [imphash for imphash in imphash_group if imphash not in set_mapping]
	for imphash in orphand_imphash_list:
		s = set()
		s.add(imphash)
		imphash_group_sets.append(s)
	
	for s in imphash_group_sets:
		print s

