# BinGroup

```
██████╗ ██╗███╗   ██╗ ██████╗ ██████╗  ██████╗ ██╗   ██╗██████╗ 
██╔══██╗██║████╗  ██║██╔════╝ ██╔══██╗██╔═══██╗██║   ██║██╔══██╗
██████╔╝██║██╔██╗ ██║██║  ███╗██████╔╝██║   ██║██║   ██║██████╔╝
██╔══██╗██║██║╚██╗██║██║   ██║██╔══██╗██║   ██║██║   ██║██╔═══╝ 
██████╔╝██║██║ ╚████║╚██████╔╝██║  ██║╚██████╔╝╚██████╔╝██║     
╚═════╝ ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝     
```                                                                

A malware classifier based on BinDiff

This program will use imphash and BinDiff to conculde the similarity of each malware. The output will show the each group's malware.

## Requirements
- Python 27
- IDA Pro
- BinDiff

## Usage
`python main.py <Folder containing malwares>`
