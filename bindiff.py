# -*- coding: utf-8 -*-
import sys, os, logging
import subprocess, yara
from enum import Enum

import models
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session, sessionmaker
from sqlalchemy import create_engine, MetaData, Column, Float, String, Integer
from sqlalchemy.ext.declarative import declarative_base

rule=yara.compile('.\ispeordll.yar')

IDA_PATH=r'C:\Users\user\Desktop\IDA6.8'
IDAQ32 = os.path.join(IDA_PATH,'idaq.exe')

BINDIFF_PATH=r'C:\Program Files\zynamics\BinDiff 4.2\bin'
DIFFER = os.path.join(BINDIFF_PATH, 'differ.exe')

BINDIFF_PYTHON_WORK_PATH = os.path.abspath(r'.\bindiff_python_workspace')

class TargetType(Enum):
	EXE = 1
	IDB = 2
	BINEXPORT = 3
	BINDIFF = 4


def check_file_exists(target):
	if not target or not os.path.isfile(target):
		raise OSError(2, 'No such file or directory', target)

def check_dir_exists(target):
	if not target or not os.path.isdir(target):
		raise OSError(2, 'No such file or directory', target)


def do_actions(args, targets):
	for target in targets:
		if os.path.isdir(target):
			for root, dirs, files in os.walk(target):
				for f in files:
				single_target = os.path.join(root, f)

				matches=rule.match(single_target)
				if not matches:
        			continue

				do_action(args, single_target)

		elif os.path.isfile(target):
			if not args.show:
				matches=rule.match(target)
				if not matches:
					logging.warning(target+' is not a valid Win32 executable.')
					continue

			do_action(args, target)

		else:
			logging.error('No such file or directory: '+target)
			continue

def do_action(args, target):
	target_type = ''
	
	matches=rule.match(target)
	if str(matches) == '[is_dll]' or str(matches) == '[is_exe]':
		target_type = TargetType.EXE
	elif str(matches) == '[is_idb]':
		target_type = TargetType.IDB
	elif target.lower().endswith('.binexport'):
		target_type = TargetType.BINEXPORT
	elif target.lower().endswith('.bindiff'):
		target_type = TargetType.BINDIFF
	else:
		logging.warning('Unregconized target: '+target)
		return
									
	if args.idb and target_type == TargetType.EXE:
		binary2idb(target)
	elif args.bin_export and target_type == TargetType.IDB:
		idb2binExport(target)
	elif args.show and target_type == TargetType.BINDIFF:
		show_bindiff_information(target)
	else:
		logging.warning('Mismatch argument and type.')
		return

def get_idb_filepath(target):
	path = os.path.dirname(target)
	basename = os.path.basename(target)
	if '.' in target:
			basename, tmp = target.rsplit('.', 1)
			basename = os.path.basename(basename)
	return os.path.join(path, basename + '.idb')

def get_binexport_filepath(target):
	filename, ext = os.path.splitext(target)
	return os.path.join(filename + '.BinExport')

'''
def get_binexport_filepath(target):
	path = os.path.dirname(target)
	basename = os.path.basename(target)
	if '.' in target:
			basename, tmp = target.rsplit('.', 1)
			basename = os.path.basename(basename)
	return os.path.join(path, basename + '.BinExport')
'''

def get_bindiff_filepath_from_binaries(target1, target2, output_dir=BINDIFF_PYTHON_WORK_PATH):
	binexport1 = get_binexport_filepath(target1)
	binexport2 = get_binexport_filepath(target2)
	return get_bindiff_filepath_from_binexports(binexport1, binexport2, output_dir)

def get_bindiff_filepath_from_binexports(binexport1, binexport2, output_dir=BINDIFF_PYTHON_WORK_PATH):
	binexport1_basename = os.path.basename(binexport1)
	binexport2_basename = os.path.basename(binexport2)

	binexport1_filename, ext = os.path.splitext(binexport1_basename)
	binexport2_filename, ext = os.path.splitext(binexport2_basename)

	return os.path.join(output_dir, binexport1_filename+'_vs_'+binexport2_filename+'.BinDiff')

def binary2idb(target):
	check_file_exists(target)
	
	matches = rule.match(target)
	if str(matches) != '[is_dll]' and str(matches) != '[is_exe]':
		error_msg = target+' is not a valid Win32 executable.'
		raise TypeError(error_msg)
		return
	
	subprocess.check_output([IDAQ32,'-B','-P+',target], shell=True)
			
	idb_file = get_idb_filepath(target)

	if os.path.isfile(idb_file):
		logging.info(target+' is converted to IDB file.')
		return idb_file
	return

def idb2binExport(target):
	check_file_exists(target)
	
	matches = rule.match(target)
	if str(matches) != '[is_idb]':
		raise TypeError(target+' is not a IDA Database file.')
		return
	
	abspath_idb_filename = get_idb_filepath(target)

	subprocess.check_output([IDAQ32,'-A','-OExporterModule:'+abspath_idb_filename,'-Sbindiff_export.idc',target], shell=True)

	binExport_file = get_binexport_filepath(target)
	
	if os.path.isfile(binExport_file):
		logging.info(target+' is converted to BinExport file.')
		return binExport_file
	
	raise RuntimeError('Cannot convert '+abspath_idb_filename+' into BinDiff file.')

def bindiff(binexport1, binexport2, output_dir=BINDIFF_PYTHON_WORK_PATH):
	check_file_exists(binexport1)

	check_file_exists(binexport2)

	if not output_dir or output_dir == '':
		output_dir = os.getcwd()
	else:
		check_dir_exists(output_dir)

	if not binexport1.endswith('.BinExport') or not binexport2.endswith('.BinExport'):
		raise TypeError(binexport1+' or '+binexport2+' is not a BinExport file.')

	#remind "XXXX"
	try:
		r = subprocess.check_output([DIFFER,'--primary',binexport1, '--secondary', binexport2, '--output_dir', output_dir], shell=True)
		#os.system('taskkill /f  /im  differ.exe')
	except subprocess.CalledProcessError as e:
		print e.output

	bindiff_file = get_bindiff_filepath_from_binexports(binexport1, binexport2, output_dir)
	if os.path.isfile(bindiff_file):
		logging.info(bindiff_file+' is generated.')
		return bindiff_file
	raise RuntimeError('No BinDiff is generated. Maybe the source filename is too long.')

def show_bindiff_information(bindiff_file):
	check_file_exists(bindiff_file)

	#raise TypeError(bindiff_file+' is not a BinDiff file.')
	
	engine = create_engine("sqlite:///"+bindiff_file)
	session = Session(engine)
	metadata_query = (session.query(models.t_metadata))
	file_query = (session.query(models.t_file))
	session.close()
	
	for file_meta in file_query:
		print 'Filename: '+file_meta.filename
		print 'MD5: '+file_meta.hash+'\n'

	print '------------------------------------------'
	
	similarity = metadata_query[0].similarity
	confidence = metadata_query[0].confidence
	print 'Similarity: {0:.2f}%'.format(similarity * 100)
	print 'Confidence: {0:.2f}%'.format(confidence * 100)

def get_similarity_from_bindiff(bindiff_file):
	check_file_exists(bindiff_file)

	engine = create_engine("sqlite:///"+bindiff_file)
	session = Session(engine)
	metadata_query = (session.query(models.t_metadata))
	#file_query = (session.query(models.t_file))
	session.close()

	return metadata_query[0].similarity

def binary2bindiff(binary1, binary2, output_dir=BINDIFF_PYTHON_WORK_PATH):
	if not os.path.isdir(output_dir):
		os.makedirs(output_dir)

	bindiff_path = get_bindiff_filepath_from_binaries(binary1, binary2, output_dir)
	if os.path.isfile(bindiff_path):
		return bindiff_path

	check_file_exists(binary1)
	check_file_exists(binary2)

	idb_file1 = get_idb_filepath(binary1)
	idb_file2 = get_idb_filepath(binary2)

	if not os.path.isfile(idb_file1):
		idb_file1 = binary2idb(binary1)

	if not os.path.isfile(idb_file2):
		idb_file2 = binary2idb(binary2)

	check_file_exists(idb_file1)

	check_file_exists(idb_file2)

	binExport_file1 = get_binexport_filepath(idb_file1)
	binExport_file2 = get_binexport_filepath(idb_file2)

	if not os.path.isfile(binExport_file1):
		binExport_file1 = idb2binExport(idb_file1)

	if not os.path.isfile(binExport_file2):
		binExport_file2 = idb2binExport(idb_file2)

	check_file_exists(binExport_file1)
	check_file_exists(binExport_file2)

	bindiff_file = get_bindiff_filepath_from_binaries(binary1, binary2, output_dir)
	if not os.path.isfile(bindiff_file):
		return bindiff(binExport_file1, binExport_file2, output_dir)

	return bindiff_file

if __name__ == '__main__':
	reload(sys)
	sys.setdefaultencoding('utf-8')

	parser = argparse.ArgumentParser(description='')
	group = parser.add_mutually_exclusive_group(required=True)
	group.add_argument('-i', '--idb', action='store_true', help='Generate idb file from BINARY')
	group.add_argument('-be', '--bin_export', action='store_true', help='Generate BinExport file from idb')
	group.add_argument('-bd', '--bindiff', action='store_true', help='Generate Bindiff files from two BinExports')
	group.add_argument('-bb', '--binary_bindiff', action='store_true', help='Generate Bindiff files from two BINARIES')
	group.add_argument('-s', '--show', action='store_true', help='Show basic information of a Bindiff file')
	parser.add_argument('target', nargs='+', help='FILE or DIRECTORY')

	args = parser.parse_args()
	targets = args.target

	if args.idb or args.bin_export or args.show:
		do_actions(args, targets)
	elif args.bindiff or args.binary_bindiff:                
		if len(targets) >= 2:
			check_file_exists(targets[0])
			check_file_exists(targets[1])

		if args.bindiff:
			method = 'bindiff'
		else:
			method = 'binary2bindiff'

		if len(targets) >= 3:
			check_dir_exists(targets[2])
			globals()[method](targets[0], targets[1], targets[2])
		else:
			globals()[method](targets[0], targets[1])
